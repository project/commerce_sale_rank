This module provides a reporting system for Drupal Commerce.

Some features that you will find in this module:

This module allows you to calculate and periodically refresh node and taxonomy 
terms sales ranks for Drupal Commerce.

Features:

* Choosing entities bundles and taxonomy terms for storing sales ranks for them.
* Setting max period in days for selecting only actual orders and line items for
  calculating sales ranks.
* Periodically refresh sales ranks by cron or manually.
* Using sales ranks as simple entity field, because it is simple numeric entity
  field.

Configuring
================================================================================

1. Download, install and enable this module.
2. Add numeric field for storing sale rank to the entity bundle and/or the 
   taxonomy vocabulary.
3. Go to the module configure page (/admin/commerce/sale_rank/admin) and choose
   necessary entities bundles and/or taxonomy vocabularies fields. Set other
   parameters.
4. Push "Refresh ranks" button and wait.
5. Configure your system cron (recommended) for periodically call url 
   "/admin/commerce/sale_rank/refresh?key=<key>".
