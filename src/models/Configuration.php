<?php

namespace Drupal\commerce_sale_rank\models;

/**
 * Module configuration.
 */
class Configuration {

  /**
   * Default max period in days for selecting only actual orders and line items.
   */
  const DEFAULT_PERIOD = 90;

  /**
   * Default multiplier for increasing rates.
   */
  const DEFAULT_MULTIPLIER = 100;

  /**
   * Default order statuses for selecting only important orders.
   */
  const DEFAULT_STATUSES = ['completed'];

  /**
   * Default cron enable status.
   */
  const DEFAULT_USE_CRON = FALSE;

  /**
   * Get configured node product fields.
   *
   * @return string[]
   *   Rank fields array, array key - node bundle.
   */
  public function getNodeFields() {
    return $this->removeEmpty(variable_get('commerce_sale_rank_node_field', []));
  }

  /**
   * Get configured taxonomy fields.
   *
   * @return string[]
   *   Rank fields array, array key - taxonomy vocabulary machine name.
   */
  public function getTaxonomyFields() {
    return $this->removeEmpty(variable_get('commerce_sale_rank_taxonomy_field', []));
  }

  /**
   * Get order statuses for selecting only important orders.
   *
   * @return string[]
   *   Order statuses.
   */
  public function getStatuses() {
    return variable_get('commerce_sale_rank_order_status', static::DEFAULT_STATUSES);
  }

  /**
   * Get max period in days for selecting only actual orders and line items.
   *
   * @return int
   *   Period.
   */
  public function getPeriod() {
    return variable_get('commerce_sale_rank_period', static::DEFAULT_PERIOD);
  }

  /**
   * Get cron enable status.
   *
   * @return bool
   *   Cron enable status.
   */
  public function getUseCron() {
    return variable_get('commerce_sale_rank_period_use_cron', static::DEFAULT_USE_CRON);
  }

  /**
   * Get key for periodically run refresh without cron.
   *
   * @return string
   *   Key.
   */
  public function getKey() {
    $key = variable_get('commerce_sale_rank_period_key', NULL);
    if (is_null($key)) {
      $key = md5(rand());
      variable_set('commerce_sale_rank_period_key', $key);
    }
    return $key;
  }

  /**
   * Get multiplier for increasing rates.
   *
   * @return int
   *   Multiplier.
   */
  public function getMulitplier() {
    return variable_get('commerce_sale_rank_multiplier', static::DEFAULT_MULTIPLIER);
  }

  /**
   * Remove empty values from array.
   *
   * @param array $values
   *   Array.
   *
   * @return array
   *   Array without empty values.
   */
  protected function removeEmpty(array $values) {
    foreach ($values as $key => $value) {
      if (empty($value)) {
        unset($values[$key]);
      }
    }
    return $values;
  }

}
