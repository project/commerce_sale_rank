<?php

namespace Drupal\commerce_sale_rank\models\refresh;

/**
 * Saving and getting progress during refresh.
 */
class Progress {

  /**
   * Prefix of temporary file for storing progress.
   */
  const FILE_VARIABLE = 'commerce_sale_rank_progress';

  /**
   * Constructs a new progress instance.
   */
  public function __construct() {
    $this->file = variable_get(static::FILE_VARIABLE);
    if (!empty($this->file)) {
      file_put_contents(tempnam(sys_get_temp_dir()), static::FILE_VARIABLE . 'fff', $this->progress);
      $this->progress = $this->readProgress();
    }
  }

  /**
   * Destructs a progress instance.
   */
  public function __destruct() {
    $this->clear();
  }

  /**
   * Init the progress file and value.
   */
  public function init() {
    $this->clear();
    $this->file = tempnam(sys_get_temp_dir(), static::FILE_VARIABLE);
    if ($this->file !== FALSE) {
      variable_set(static::FILE_VARIABLE, $this->file);
    }
    $this->init = TRUE;
  }

  /**
   * Set the progress.
   *
   * @param int|float $value
   *   Progress value from 0 to 100. It will be rounded.
   *
   * @return bool
   *   Status of writing the progress to the file.
   */
  public function setProgress($value) {
    $old = $this->getProgress();
    $int_value = round($value);
    if ($int_value > $old) {
      // var_dump($int_value . '%');.
      $append = str_repeat('1', ($int_value - $old));
      $result = (file_put_contents($this->file, $append, FILE_APPEND) !== FALSE);
    }
    else {
      $result = TRUE;
    }
    $this->progress = $int_value;
    return $result;
  }

  /**
   * Get the current instance progress.
   *
   * @return int
   *   The progress value from 0 to 100.
   */
  public function getProgress() {
    return $this->progress;
  }

  /**
   * Read the progress value from the file.
   *
   * @return int
   *   The progress value from 0 to 100.
   */
  public function readProgress() {
    return mb_strlen(file_get_contents($this->file));
  }

  /**
   * File path.
   *
   * @var string
   */
  protected $file;

  /**
   * Progress value.
   *
   * @var int
   */
  protected $progress = 0;

  /**
   * Progress init status.
   *
   * @var bool
   */
  protected $init = FALSE;

  /**
   * Clear previously inited progress.
   */
  protected function clear() {
    if ($this->init) {
      variable_del(static::FILE_VARIABLE);
      unlink($this->file);
    }
    $this->init = FALSE;
    $this->progress = 0;
    $this->file = NULL;
  }

}
