<?php

namespace Drupal\commerce_sale_rank\models\refresh;

use Drupal\commerce_sale_rank\models\Configuration;

/**
 * Main model for refreshing sales ranks of all available entities.
 */
class Model {

  /**
   * Refresh sales ranks of all available entities. May take a long time.
   *
   * @return bool
   *   Refresh status.
   */
  public function refresh() {
    ini_set('max_execution_time', 0);
    set_time_limit(0);
    $progress = new Progress();
    $progress->init();
    $result = TRUE;
    $objects = $this->createRefreshObjects($progress);
    foreach ($objects as $object) {
      $result = $object->refresh();
      if ($result === FALSE) {
        break;
      }
    }
    unset($progress);
    return $result;
  }

  /**
   * Create array of objects for refreshing entities of each type.
   *
   * @param Progress $progress
   *   Progress object.
   *
   * @return Entity[]
   *   Array of objects for refreshing entities of each type.
   */
  public function createRefreshObjects(Progress $progress) {
    $progress->init();
    $conf = new Configuration();
    $node_fields = $conf->getNodeFields();
    $term_fields = $conf->getTaxonomyFields();
    $period = $conf->getPeriod();
    $multiplier = $conf->getMulitplier();
    $statuses = $conf->getStatuses();
    $total = count($node_fields) + count($term_fields);
    $result = [];
    $result[] = new Product($progress, $period, $multiplier, $statuses, $total, $node_fields);
    $result[] = new Taxonomy($progress, $period, $multiplier, $statuses, $total, $term_fields);
    return $result;
  }

}
