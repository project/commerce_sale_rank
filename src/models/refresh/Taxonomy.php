<?php

namespace Drupal\commerce_sale_rank\models\refresh;

/**
 * Refreshable entity - taxonomy term.
 */
class Taxonomy extends Entity {

  /**
   * Refresh terms by bundle name.
   *
   * @param string $bundle
   *   Bundle.
   * @param string $rank_field
   *   Bundle field for storing rank value.
   * @param string $max_progress
   *   Max progress, which can be reached by refreshing bundle entities.
   *
   * @return bool
   *   Refresh status.
   */
  protected function refreshBundle($bundle, $rank_field, $max_progress) {
    $node_bundles = array_keys(commerce_product_types());
    $fields = $this->getFields('node', $node_bundles, 'commerce_product_reference');
    $ranks = [];
    foreach ($fields as $field) {
      $ranks = $this->sumRanks($ranks, $this->getBundleProductRanks($bundle, $node_bundles, $field['relation'], $field['field']));
    }
    $result = $this->updateRanks($ranks, $rank_field, $max_progress);
    return $result;
  }

  /**
   * Get ranks of terms for product field.
   *
   * @param string $bundle
   *   Bundle.
   * @param string[] $node_bundles
   *   Node bundles which contain product references.
   * @param string $product_relation
   *   Product field relation name.
   * @param string $product_field
   *   Product field relation field name.
   *
   * @return array
   *   Array of ranks. Array key - term id. Each item conains fields: <br>
   *    sum - total rank value (see getSumExpression function), <br>
   *    count - count of products which have references to the term.
   */
  protected function getBundleProductRanks($bundle, array $node_bundles, $product_relation, $product_field) {
    $fields = $this->getTaxonomyFields($node_bundles, $bundle);
    $ranks = [];
    foreach ($fields as $field) {
      $ranks = $this->sumRanks($ranks, $this->getBundleProductField($bundle, $node_bundles, $product_relation, $product_field, $field['relation'], $field['field']));
    }
    return $ranks;
  }

  /**
   * Get ranks of terms for product field.
   *
   * @param string $bundle
   *   Bundle.
   * @param string[] $node_bundles
   *   Node bundles which contain product references.
   * @param string $product_relation
   *   Product field relation name.
   * @param string $product_field
   *   Product field relation field name.
   * @param string $product_taxonomy_relation
   *   Taxonomy field relation name.
   * @param string $product_taxonomy_field
   *   Taxonomy field relation field name.
   *
   * @return array
   *   Ranks. Array key - term id. Each item conains fields: <br>
   *    sum - total rank value (see getSumExpression function), <br>
   *    count - count of products which have references to the term.
   */
  protected function getBundleProductField($bundle, array $node_bundles, $product_relation, $product_field, $product_taxonomy_relation, $product_taxonomy_field) {
    $query = $this->createQuery($bundle, $node_bundles, $product_relation, $product_field, $product_taxonomy_relation, $product_taxonomy_field);
    $vocabulary_object = taxonomy_vocabulary_machine_name_load($bundle);
    $terms = taxonomy_get_tree($vocabulary_object->vid);
    $ranks = [];
    foreach ($terms as $term) {
      $term_query = clone $query;
      $term_query->condition('taxonomy_term_data.tid', $term->tid);
      $rank = $term_query->execute()->fetch();
      if ($rank->rank_count > 0) {
        $ranks[$term->tid]['count'] = $rank->rank_count;
        $ranks[$term->tid]['sum'] = is_null($rank->rank_sum) ? 0 : $rank->rank_sum;
      }
      else {
        $ranks[$term->tid] = ['sum' => 0, 'count' => 0];
      }
    }
    return $this->calculateTaxonomyRank($terms, $ranks);
  }

  /**
   * Get array of taxonomy fields for node bundles.
   *
   * @param string[] $bundles
   *   Node bundles.
   * @param string $vocabulary
   *   Vocabulary machine name.
   *
   * @return array
   *   Field(s), which were founded. Field properties: <br>
   *    relation - relation name for storing field, <br>
   *    field - field name in the relation, <br>
   *    settings - field settings.
   */
  protected function getTaxonomyFields(array $bundles, $vocabulary) {
    $fields = $this->getFields('node', $bundles, 'taxonomy_term_reference');
    $result = [];
    foreach ($fields as $field) {
      foreach ($field['settings']['allowed_values'] as $allowed_vocabulary) {
        if ($allowed_vocabulary['vocabulary'] == $vocabulary) {
          $result[] = $field;
          break;
        }
      }
    }
    return $result;
  }

  /**
   * Create query for selecting taxonomy terms ranks.
   *
   * @param string $bundle
   *   Bundle.
   * @param string[] $node_bundles
   *   Node bundles which contain product references.
   * @param string $product_relation
   *   Product field relation name.
   * @param string $product_field
   *   Product field relation field name.
   * @param string $product_taxonomy_relation
   *   Taxonomy field relation name.
   * @param string $product_taxonomy_field
   *   Taxonomy field relation field name.
   *
   * @return \SelectQuery
   *   Query object.
   */
  protected function createQuery($bundle, array $node_bundles, $product_relation, $product_field, $product_taxonomy_relation, $product_taxonomy_field) {
    $query = db_select('taxonomy_term_data');
    $query->join('taxonomy_vocabulary', NULL, 'taxonomy_vocabulary.vid = taxonomy_term_data.vid and taxonomy_vocabulary.machine_name = :name', [':name' => $bundle]);
    $query->leftJoin($product_taxonomy_relation, NULL, "taxonomy_term_data.tid = $product_taxonomy_relation.$product_taxonomy_field and $product_taxonomy_relation.entity_type = :entity_type and $product_taxonomy_relation.bundle in (:bundles)", [':entity_type' => 'node', ':bundles' => $node_bundles]);
    $query->leftJoin('node', NULL, "node.nid = $product_taxonomy_relation.entity_id");
    $query->leftJoin($product_relation, NULL, "$product_relation.entity_type = :entity_type and $product_relation.bundle = node.type and $product_relation.entity_id = node.nid", [':entity_type' => 'node']);

    $this->leftJoinCommerceLineItem($query, $product_relation, $product_field);

    $query->groupBy('taxonomy_term_data.tid');
    $query->orderBy('taxonomy_term_data.tid');

    $query->fields('taxonomy_term_data', ['tid']);
    $query->addExpression($this->getSumExpression(), 'rank_sum');
    $query->addExpression('count(distinct node.nid)', 'rank_count');
    return $query;
  }

  /**
   * Calculate terms rank according to each term hierarchy.
   *
   * @param \stdClass[] $terms
   *   Terms.
   * @param array $ranks
   *   Ranks. Array key - term id. Each item conains fields: <br>
   *    sum - total rank value (see getSumExpression function), <br>
   *    count - count of products which have references to the term.
   *
   * @return array[]
   *   Ranks which were transformed according to hierarchy. Array key - term id.
   *   Each item conains fields: <br>
   *    sum - total rank value (see getSumExpression function), <br>
   *    count - count of products which have references to the term.
   */
  protected function calculateTaxonomyRank(array $terms, array $ranks) {
    $depth = 0;
    foreach ($terms as $term) {
      if ($term->depth > $depth) {
        $depth = $term->depth;
      }
    }
    for ($i = $depth; $i > 0; $i--) {
      foreach ($terms as $term) {
        if ($term->depth == $depth) {
          foreach ($term->parents as $parent) {
            $ranks[$parent]['sum'] += $ranks[$term->tid]['sum'];
            $ranks[$parent]['count'] += $ranks[$term->tid]['count'];
          }
        }
      }
    }
    return $ranks;
  }

  /**
   * Sum one sale rank to another.
   *
   * @param array $first
   *   First sale rank. Array key - term id. Item must conain fields: <br>
   *    sum - total rank value (see getSumExpression function), <br>
   *    count - count of products which have references to the term.
   * @param array $second
   *   Second sale rank. Array key - term id. Item must conain fields: <br>
   *    sum - total rank value (see getSumExpression function), <br>
   *    count - count of products which have references to the term.
   *
   * @return array
   *   Result (second) sale rank. Array key - term id.
   *   Each item conains fields: <br>
   *    sum - total rank value (see getSumExpression function), <br>
   *    count - count of products which have references to the term.
   */
  protected function sumRanks(array $first, array $second) {
    foreach ($first as $tid => $rank) {
      if (isset($second[$tid])) {
        $second[$tid]['sum'] += $rank['sum'];
        $second[$tid]['count'] += $rank['count'];
      }
      else {
        $second[$tid] = $rank;
      }
    }
    return $second;
  }

  /**
   * Update (save) sales ranks.
   *
   * @param array $ranks
   *   Ranks. Array key - term id. Each item conains fields: <br>
   *    sum - total rank value (see getSumExpression function), <br>
   *    count - count of products which have references to the term.
   * @param string $field
   *   Term field name for storing sale rank value.
   * @param int $max_progress
   *   Max progress, which can be reached by refreshing terms.
   *
   * @return bool
   *   Update status.
   */
  protected function updateRanks(array $ranks, $field, $max_progress) {
    $result = TRUE;
    $init_progress = $this->progress->getProgress();
    $count = count($ranks);
    $i = 1;
    foreach ($ranks as $tid => $rank) {
      if ($rank['sum'] == 0 || $rank['count'] == 0) {
        $rank_value = NULL;
      }
      else {
        $rank_value = round($rank['sum'] / $rank['count']);
      }
      $result = $this->updateRank($tid, $field, $rank_value);
      if ($result === FALSE) {
        break;
      }
      $this->progress->setProgress($init_progress + (float) $i * ($max_progress - $init_progress) / $count);
      $i++;
    }
    return $result;
  }

  /**
   * Update (save) single sale rank.
   *
   * @param int $tid
   *   Term id.
   * @param string $field
   *   Term field name for storing sale rank value.
   * @param int|null $value
   *   Sale rank value.
   *
   * @return bool
   *   Update status.
   */
  protected function updateRank($tid, $field, $value) {
    $entity = entity_metadata_wrapper('taxonomy_term', $tid);
    if ($entity->{$field}->value() != $value) {
      $entity->{$field}->set($value);
      $result = $entity->save();
    }
    else {
      $result = TRUE;
    }
    return $result;
  }

  /**
   * Get sql expression for selecting total rank value.
   *
   * In general case, rank value will be selected from commerce line item and
   * commerce order.
   *
   * @return string
   *   Sql expression.
   */
  protected function getSumExpression() {
    $last = $this->getMaxDateTime()->getTimestamp();
    $now = (new \DateTime)->getTimestamp();
    $expression = ' sum(';
    $expression .= ' case when commerce_order.order_id is null or commerce_line_item.order_id is null then 0 else';
    $expression .= ' commerce_line_item.quantity*' . $this->multiplier;
    $expression .= ' *(commerce_line_item.created - ' . $last . ')';
    $expression .= " /" . ($now - $last);
    $expression .= ' end';
    $expression .= ' )';
    return $expression;
  }

}
