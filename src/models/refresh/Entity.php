<?php

namespace Drupal\commerce_sale_rank\models\refresh;

/**
 * Refreshable entity.
 */
abstract class Entity {

  const PORTION = 1000;

  /**
   * Constructs a new archiver instance.
   *
   * @param \Drupal\commerce_sale_rank\models\refresh\Progress $progress
   *   Progress object.
   * @param int $period
   *   Max period in days for selecting only actual orders and line items.
   * @param int $multiplier
   *   Multiplier for increasing rates.
   * @param string[] $statuses
   *   Order statuses for selecting only important orders.
   * @param int $total
   *   Total number of refreshable entities for correct progress.
   * @param string[] $fields
   *   Field name for each entity bundle for storing sale rate.
   */
  public function __construct(Progress $progress, $period, $multiplier, array $statuses, $total, array $fields) {
    $this->progress = $progress;
    $this->period = $period;
    $this->statuses = $statuses;
    $this->total = $total;
    $this->fields = $fields;
    $this->multiplier = $multiplier;
  }

  /**
   * Refresh all sales ranks for entities.
   *
   * @return bool
   *   Refresh status.
   */
  public function refresh() {
    $result = TRUE;
    $init_progress = $this->progress->getProgress();
    $max_progress = $this->getMaxProgress();
    $divider = count($this->fields);
    $i = 1;
    foreach ($this->fields as $bundle => $rank_field) {
      $result = $this->refreshBundle($bundle, $rank_field, $init_progress + $i * ($max_progress - $init_progress) / $divider);
      if ($result === FALSE) {
        break;
      }
      $i++;
    }
    if ($result) {
      $this->progress->setProgress($max_progress);
    }
    else {
      $this->progress->setProgress($init_progress);
    }
    return $result;
  }

  /**
   * Progress object.
   *
   * @var Progress
   */
  protected $progress;

  /**
   * Max period in days for selecting only actual orders and line items.
   *
   * @var int
   */
  protected $period;

  /**
   * Order statuses for selecting only important orders.
   *
   * @var string[]
   */
  protected $statuses;

  /**
   * Total number of refreshable entities for correct progress.
   *
   * @var int
   */
  protected $total;

  /**
   * Field name for each entity bundle for storing sale rate.
   *
   * @var string[]
   */
  protected $fields;

  /**
   * Multiplier for increasing rating.
   *
   * @var int
   */
  protected $multiplier;

  /**
   * Refresh entities by bundle name.
   *
   * @param string $bundle
   *   Bundle.
   * @param string $rank_field
   *   Bundle field for storing rank value.
   * @param string $max_progress
   *   Max progress, which can be reached by refreshing bundle entities.
   *
   * @return bool
   *   Refresh status.
   */
  abstract protected function refreshBundle($bundle, $rank_field, $max_progress);

  /**
   * Get max progress, which can be reached by refreshing entities.
   *
   * @return int
   *   Progress value from 0 to 100 percents.
   */
  protected function getMaxProgress() {
    return round($this->progress->getProgress() + 100 * count($this->fields) / $this->total);
  }

  /**
   * Get max datetime for selecting only actual orders and line items.
   *
   * @return \DateTime
   *   Object with datetime.
   */
  protected function getMaxDateTime() {
    $now = new \DateTime();
    $now->sub(new \DateInterval('P' . $this->period . 'D'));
    return $now;
  }

  /**
   * Get fields for entity type.
   *
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle
   *   Bundle.
   * @param string $type
   *   Field type.
   * @param bool $first
   *   Return first field or all fields.
   *
   * @return array
   *   Field(s), which were founded. Field properties: <br>
   *    relation - relation name for storing field, <br>
   *    field - field name in the relation, <br>
   *    settings - field settings.
   */
  protected function getFields($entity_type, $bundle, $type, $first = FALSE) {
    $fields = field_read_fields([
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'type' => $type,
    ]);
    $result_fields = [];
    foreach ($fields as $properties) {
      $result_fields[] = $this->getFieldInfo($properties['field_name']);
    }
    if ($first) {
      $result = reset($result_fields);
    }
    else {
      $result = $result_fields;
    }
    return $result;
  }

  /**
   * Join commerce_line_item and order relations.
   *
   * @param \SelectQueryInterface $query
   *   Query to join.
   * @param string $product_relation
   *   Product relation name.
   * @param string $product_field
   *   Product relation field name.
   */
  protected function leftJoinCommerceLineItem(\SelectQueryInterface $query, $product_relation, $product_field) {
    $last = $this->getMaxDateTime()->getTimestamp();
    $field_commerce_product = $this->getFields('commerce_line_item', 'product', 'commerce_product_reference', TRUE);
    $relation = $field_commerce_product['relation'];
    $field = $field_commerce_product['field'];
    $query->leftJoin($field_commerce_product['relation'], NULL, "$product_relation.$product_field = $relation.$field and $relation.entity_type = :type", [':type' => 'commerce_line_item']);
    $query->leftJoin('commerce_line_item', NULL, "$relation.entity_id = commerce_line_item.line_item_id and commerce_line_item.created >= :time", [':time' => $last]);
    $query->leftJoin('commerce_order', NULL, 'commerce_line_item.order_id = commerce_order.order_id and commerce_order.created >= :time and commerce_order.status in (:statuses)', [':time' => $last, ':statuses' => $this->statuses]);
  }

  /**
   * Get required for join field info.
   *
   * @param string $field_name
   *   Field name.
   *
   * @return array
   *   Field properties:
   *    relation - relation name for storing field,
   *    field - field name in the relation,
   *    settings - field settings.
   */
  protected function getFieldInfo($field_name) {
    $field_info = field_info_field($field_name);
    $relation = $field_info['storage']['details']['sql'][FIELD_LOAD_CURRENT];
    $field = reset($relation);
    $result = [
      'relation' => key($relation),
      'field' => reset($field),
      'settings' => $field_info['settings'],
    ];
    return $result;
  }

}
