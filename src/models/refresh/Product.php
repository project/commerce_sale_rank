<?php

namespace Drupal\commerce_sale_rank\models\refresh;

/**
 * Refreshable entity - product.
 */
class Product extends Entity {

  /**
   * Refresh products by bundle name.
   *
   * @param string $bundle
   *   Bundle.
   * @param string $rank_field
   *   Bundle field for storing rank value.
   * @param string $max_progress
   *   Max progress, which can be reached by refreshing bundle entities.
   *
   * @return bool
   *   Refresh status.
   */
  protected function refreshBundle($bundle, $rank_field, $max_progress) {
    $fields = $this->getFields('node', $bundle, 'commerce_product_reference');
    $reset = TRUE;
    $result = TRUE;
    $divider = count($fields);
    $init_progress = $this->progress->getProgress();
    $i = 1;
    foreach ($fields as $field) {
      $result = $this->refreshBundleProduct($bundle, $rank_field, $field['relation'], $field['field'], $init_progress + $i * ($max_progress - $init_progress) / $divider, $reset);
      if ($result === FALSE) {
        break;
      }
      $reset = FALSE;
      $i++;
    }
    return $result;
  }

  /**
   * Refresh products by bundle name and entity product field.
   *
   * @param string $bundle
   *   Bundle.
   * @param string $rank_field
   *   Bundle field for storing rank value.
   * @param string $product_relation
   *   Bundle relation for storing product reference.
   * @param string $product_field
   *   Bundle relation field for storing product reference.
   * @param int $max_progress
   *   Max progress, which can be reached by refreshing bundle entities for
   *   product field.
   * @param bool $reset
   *   Reset or append rank value to entities.
   *
   * @return bool
   *   Refresh status.
   */
  protected function refreshBundleProduct($bundle, $rank_field, $product_relation, $product_field, $max_progress, $reset = TRUE) {
    $result = TRUE;
    $query = $this->createQuery($bundle, $product_relation, $product_field);
    $count = $query->countQuery()->execute()->fetchField();
    for ($i = 0; $i < $count; $i += static::PORTION) {
      $query->range($i, static::PORTION);
      $ranks = $query->execute();
      $result = $this->updateRanks($ranks, $rank_field, $i, $count, $max_progress, $reset);
      if ($result === FALSE) {
        break;
      }
    }
    $this->progress->setProgress($max_progress);
    return $result;
  }

  /**
   * Create query for selecting entities sales ranks.
   *
   * @param string $bundle
   *   Bundle.
   * @param string $product_relation
   *   Bundle relation for storing product reference.
   * @param string $product_field
   *   Bundle relation field for storing product reference.
   *
   * @return \SelectQuery
   *   Query object.
   */
  protected function createQuery($bundle, $product_relation, $product_field) {
    $query = db_select($product_relation);
    $this->leftJoinCommerceLineItem($query, $product_relation, $product_field);
    $query->condition($product_relation . '.bundle', $bundle);

    $query->groupBy($product_relation . '.entity_id');
    $query->orderBy($product_relation . '.entity_id');

    $query->fields($product_relation, ['entity_id']);
    $query->addExpression($this->getExpression(), 'rank');
    return $query;
  }

  /**
   * Update sales ranks.
   *
   * @param \DatabaseStatementInterface $ranks
   *   Sales ranks objects with "entity_id" and "rank" fields.
   * @param string $rank_field
   *   Entity field for storing rank value.
   * @param int $offset
   *   Query offset for evaluating progress.
   * @param int $count
   *   Query count for evaluating progress.
   * @param int $max_progress
   *   Max progress, which can be reached by refreshing entities.
   * @param bool $reset
   *   Reset or append rank value to entities.
   *
   * @return bool
   *   Update status.
   */
  protected function updateRanks(\DatabaseStatementInterface $ranks, $rank_field, $offset, $count, $max_progress, $reset) {
    $j = 0;
    $result = TRUE;
    $init_progress = $this->progress->getProgress();
    foreach ($ranks as $rank) {
      $j++;
      $result = $this->updateRank($rank, $rank_field, $reset);
      if ($result === FALSE) {
        break;
      }
      else {
        $this->progress->setProgress($init_progress + (float) ($offset + $j) * ($max_progress - $init_progress) / $count);
      }
    }
    return $result;
  }

  /**
   * Update sale rank.
   *
   * @param \stdClass $rank
   *   Sale rank object with "entity_id" and "rank" fields.
   * @param string $rank_field
   *   Entity field for storing rank value.
   * @param bool $reset
   *   Reset or append rank value to entities.
   *
   * @return bool
   *   Update status.
   */
  protected function updateRank(\stdClass $rank, $rank_field, $reset) {
    $entity = entity_metadata_wrapper('node', $rank->entity_id);
    $old_value = $entity->{$rank_field}->value();
    $rank_value = $this->calculateRank($old_value, $rank->rank, $reset);
    if ($old_value <> $rank_value) {
      $entity->{$rank_field}->set($rank_value);
      $result = $entity->save();
    }
    else {
      $result = TRUE;
    }
    return $result;
  }

  /**
   * Calculate new sale rank value by existing value and new sale rank value.
   *
   * @param int|null $old_value
   *   Existing sale rank value or null if it is not set.
   * @param int|null $rank
   *   New sale rank value or null if it is not set.
   * @param bool $reset
   *   Reset or append rank value to entities.
   *
   * @return int|null
   *   Rank value, may be null if sale rank is empty.
   */
  protected function calculateRank($old_value, $rank, $reset) {
    if (empty($rank)) {
      if ($reset) {
        $rank_value = NULL;
      }
      else {
        $rank_value = $old_value;
      }
    }
    else {
      if ($reset || empty($old_value)) {
        $rank_value = round($rank);
      }
      else {
        $rank_value = round($rank + $old_value);
      }
    }
    return $rank_value;
  }

  /**
   * Get sql expression for selecting rank from commerce line item and order.
   *
   * @return string
   *   Sql expression.
   */
  protected function getExpression() {
    $last = $this->getMaxDateTime()->getTimestamp();
    $now = (new \DateTime)->getTimestamp();
    $expression = ' sum(';
    $expression .= ' case when commerce_order.order_id is null or commerce_line_item.order_id is null then 0 else ';
    $expression .= ' commerce_line_item.quantity*' . $this->multiplier;
    $expression .= ' *(commerce_line_item.created - ' . $last . ')';
    $expression .= " /" . ($now - $last);
    $expression .= ' end ';
    $expression .= ' )';
    return $expression;
  }

}
