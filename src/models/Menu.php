<?php

namespace Drupal\commerce_sale_rank\models;

/**
 * Menu items info.
 */
class Menu {

  /**
   * Get menu items.
   *
   * @return array
   *   Menu items array for use in hook_menu().
   */
  public function getItems() {
    return array_merge($this->getAdminItems(), $this->getRefreshProgressItems(), $this->getRefreshItems());
  }

  /**
   * Get module admin menu items.
   *
   * @return array
   *   Menu items array for use in hook_menu().
   */
  public function getAdminItems() {
    $items = [];
    $items['admin/commerce/sale_rank/admin'] = [
      'title' => t('Commerce sale rank admin'),
      'page callback' => 'commerce_sale_rank_admin',
      'access arguments' => array('access administration pages'),
      'type' => MENU_CALLBACK,
    ];
    return $items;
  }

  /**
   * Get refresh progress menu items.
   *
   * @return array
   *   Menu items array for use in hook_menu().
   */
  public function getRefreshProgressItems() {
    $items = [];
    $items['admin/commerce/sale_rank/refresh_progress'] = [
      'title' => 'Refresh progress',
      'description' => 'Refresh progress',
      'page callback' => 'commerce_sale_rank_refresh_progress',
      'access arguments' => array('access administration pages'),
      'type' => MENU_CALLBACK,
    ];
    return $items;
  }

  /**
   * Get refresh items menu items.
   *
   * @return array
   *   Menu items array for use in hook_menu().
   */
  public function getRefreshItems() {
    $items = [];
    $conf = new Configuration();
    if (!$conf->getUseCron()) {
      $items['admin/commerce/sale_rank/refresh'] = [
        'title' => t('Refresh sales ranks.'),
        'page callback' => 'commerce_sale_rank_refresh_refresh',
      // Any user can launch this operation (can be used in cron)
        'access callback' => TRUE,
        'type' => MENU_CALLBACK,
      ];
    }
    return $items;
  }

}
