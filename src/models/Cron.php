<?php

namespace Drupal\commerce_sale_rank\models;

/**
 * Cron commands info.
 */
class Cron {

  /**
   * Get cron commands if cron is enabled.
   *
   * @return array
   *   Commands array for use in hook_queue_info().
   */
  public function getQueueCommands() {

    $conf = new Configuration();
    $queues = [];
    if ($conf->getUseCron()) {
      $queues['commerce_sale_rank_refresh_refresh'] = [
        'worker callback' => 'commerce_sale_rank_refresh_refresh',
        'time' => 3000,
      ];
    }
    return $queues;
  }

}
