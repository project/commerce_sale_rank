<?php

namespace Drupal\commerce_sale_rank\views;

/**
 * Form for refresh entities sales ranks.
 */
class Refresh {

  /**
   * Get form.
   *
   * @return array
   *   Form array for use in drupal_get_form().
   */
  public function getForm() {
    $form = [];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Refresh ranks',
    ];

    return $form;
  }

}
