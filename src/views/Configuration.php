<?php

namespace Drupal\commerce_sale_rank\views;

use Drupal\commerce_sale_rank\models\Configuration as Model;

/**
 * Configuration form view.
 */
class Configuration {

  /**
   * Get configuration form.
   *
   * @return array
   *   Form array for use in drupal_get_form().
   */
  public function getForm() {
    $form = array();
    $this->attachPeriod($form);
    $this->attachMultiplier($form);
    $this->attachUseCron($form);
    $this->attachKey($form);
    $this->attachNodeFields($form);
    $this->attachTaxonomyFields($form);
    $this->attachStatuses($form);
    return system_settings_form($form);
  }

  /**
   * Attach node with product fields.
   *
   * @param array $form
   *   Form.
   */
  public function attachNodeFields(array &$form) {
    $types = commerce_product_types();
    $empty_bundles = [];
    $model = new Model();
    $node_fields = $model->getNodeFields();
    $form['commerce_sale_rank_node_field'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Commerce product nodes'),
    ];
    foreach ($types as $type => $info) {
      $options = $this->getTypeFields('node', $type);
      if (empty($options)) {
        $empty_bundles[] = $info['name'];
      }
      else {
        $form['commerce_sale_rank_node_field'][$type] = $this->getNodeSelect($type, $info['name'], $info['description'], $options, $node_fields);
      }
    }
    $this->attachEmptyBundlesMessage($form['commerce_sale_rank_node_field'], $empty_bundles);
  }

  /**
   * Attach taxonomy vocabularies.
   *
   * @param array $form
   *   Form.
   */
  public function attachTaxonomyFields(array &$form) {
    $types = taxonomy_get_vocabularies();
    $empty_bundles = [];
    $model = new Model();
    $node_fields = $model->getTaxonomyFields();
    $form['commerce_sale_rank_taxonomy_field'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Taxonomy vocabularies'),
    ];
    foreach ($types as $info) {
      $options = $this->getTypeFields('taxonomy_term', $info->machine_name);
      if (empty($options)) {
        $empty_bundles[] = $info->name;
      }
      else {
        $form['commerce_sale_rank_taxonomy_field'][$info->machine_name] =
          $this->getNodeSelect($info->machine_name, $info->name, $info->description, $options, $node_fields);
      }
    }
    $this->attachEmptyBundlesMessage($form['commerce_sale_rank_taxonomy_field'], $empty_bundles);
  }

  /**
   * Get node rank field select for use in form api.
   *
   * @param string $type
   *   Node bundle.
   * @param string $name
   *   Node bundle name.
   * @param string $description
   *   Node bundle description.
   * @param string[] $options
   *   Field list available for use for storing rank value.
   * @param string[] $node_fields
   *   Already configured node fields for storing rank value.
   *   Array key - bundle, value - field name.
   *
   * @return array
   *   Select array for use in form api.
   */
  public function getNodeSelect($type, $name, $description, array $options, array $node_fields) {
    $select = [
      '#type' => 'select',
      '#title' => $name,
      '#options' => $options,
      '#description' => $description,
      '#required' => FALSE,
    ];
    if (isset($node_fields[$type])) {
      $select['#default_value'] = $node_fields[$type];
    }
    return $select;
  }

  /**
   * Get fields available for storing rank value.
   *
   * @param string $type
   *   Entity type.
   * @param string $bundle
   *   Entity bundle.
   *
   * @return array
   *   Fields: key - field name, value - field label.
   */
  public function getTypeFields($type, $bundle) {

    $fields = field_read_fields([
      'entity_type' => $type,
      'bundle' => $bundle,
      'type' => ['number_integer', 'number_decimal'],
    ]);
    if (empty($fields)) {
      $options = [];
    }
    else {
      $options = [t('Disabled')];
      foreach ($fields as $info) {
        $field_info = field_info_instance($type, $info['field_name'], $bundle);
        $options[$info['field_name']] = $field_info['label'];
      }
    }
    return $options;
  }

  /**
   * Attach order statuses.
   *
   * @param array $form
   *   Form.
   */
  public function attachStatuses(array &$form) {
    $model = new Model();
    $select = $this->getOrderStatusesSelect();
    $select['#description'] = t('Only orders with these statuses will be counted.');
    $select['#default_value'] = $model->getStatuses();
    $form['commerce_sale_rank_order_status'] = $select;
  }

  /**
   * Get select for order statuses.
   *
   * @return array
   *   Select for use in form api.
   */
  public function getOrderStatusesSelect() {
    $statuses = commerce_order_status_options_list();
    $status_options = [];
    $size = 1;
    foreach ($statuses as $status_group_title => $status_group) {
      $options = [];
      foreach ($status_group as $status_name => $status_title) {
        $options[$status_name] = $status_title;
        $size++;
      }
      if (count($options) > 1) {
        $status_options[$status_group_title] = $options;
        $size += 1;
      }
      else {
        $status_options = array_merge($status_options, $options);
      }
    }
    return $this->getMultipleSelect($status_options, $size, t('Order status'));
  }

  /**
   * Attach period.
   *
   * @param array $form
   *   Form.
   */
  public function attachPeriod(array &$form) {
    $model = new Model();
    $form['commerce_sale_rank_period'] = [
      '#type' => 'textfield',
      '#default_value' => $model->getPeriod(),
      '#title' => t('Period'),
      '#description' => t('Period in days, all orders older than that period will be ignored.'),
    ];
  }

  /**
   * Attach cron enabled.
   *
   * @param array $form
   *   Form.
   */
  public function attachUseCron(array &$form) {
    $model = new Model();
    $form['commerce_sale_rank_period_use_cron'] = [
      '#type' => 'checkbox',
      '#title' => t('Use drupal cron for refresh.'),
      '#description' => t("If not checked then you should call admin/commerce/sale_rank/refresh?key=&lt;key&gt; yourself."),
      '#default_value' => $model->getUseCron(),
    ];
  }

  /**
   * Attach key.
   *
   * @param array $form
   *   Form.
   */
  public function attachKey(array &$form) {
    $model = new Model();
    $form['commerce_sale_rank_period_key'] = [
      '#type' => 'textfield',
      '#title' => t('Key when you do not use system cron'),
      '#default_value' => $model->getKey(),
      '#required' => TRUE,
    ];
  }

  /**
   * Attach multiplier.
   *
   * @param array $form
   *   Form.
   */
  public function attachMultiplier(array &$form) {
    $model = new Model();
    $form['commerce_sale_rank_multiplier'] = [
      '#type' => 'textfield',
      '#default_value' => $model->getMulitplier(),
      '#title' => t('Multiplier'),
      '#description' => t('Multiplier for increasing rating.'),
    ];
  }

  /**
   * Get multiple select.
   *
   * @param string[] $options
   *   Select options.
   * @param int $size
   *   Select size.
   * @param string $title
   *   Select title.
   *
   * @return array
   *   Select array for use in form api.
   */
  protected function getMultipleSelect(array $options, $size, $title) {
    $select = [
      '#type' => 'select',
      '#title' => $title,
      '#size' => $size,
      '#multiple' => TRUE,
      '#options' => $options,
      '#required' => TRUE,
    ];
    return $select;
  }

  /**
   * Attach message for empty bundles.
   *
   * @param array $form
   *   Form.
   * @param string[] $bundles
   *   Bundles names.
   */
  protected function attachEmptyBundlesMessage(array &$form, array $bundles) {
    if (!empty($bundles)) {
      $form['empty_bundles'] = [
        '#markup' => t('There is no number fields for bundles:') . ' ' . implode(', ', $bundles) . '.',
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      ];
    }
  }

}
